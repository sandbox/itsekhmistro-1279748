// $Id$

AJAX Pagination history

-------------------------------------------------------------------------------

This module allows to use Ajax Pagination on your Drupal site without breaking Browsers history navigation buttons.

Module was built using Jquery history plugin:
http://tkyk.github.com/jquery-history-plugin/

INSTALLATION:
-------------------------------------------------------------------------------

Install as usual, see http://drupal.org/node/70151 for further information.

USAGE:

Call ajax_pager_history_add() on every page where you would like to have AJAX Pagination history.


Maintainers
-------------------------------------------------------------------------------

Current maintainer: Ivan Tsekhmistro - itsekhmistro@adyax.com

