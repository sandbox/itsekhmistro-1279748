(function($){
  function paginatorHistory(hash) {
    if(hash != "") {
      $($('.pager-item a[href$="' + hash +'"]').get(0)).click();
    } else {
      $($('.pager-first a').get(0)).click();
    }
  }

  $(document).ready(function() {
    $.history.init(paginatorHistory, {unescape:"/=;"});
  });
  
  Drupal.behaviors.AJAXPagerHistory = function(){
    $('ul.pager li a').click(function(e) {
                      var url = $(this).attr('href');
                      url = url.replace(/^.*\?/, '');
                      //url = url.replace(/^.*=/, '');                    
                      $.history.load(url);
                      return false;
                  });
  }
})(jQuery);